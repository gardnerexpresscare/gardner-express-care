Gardner Express Care offers quick-affordable Urgent Care visits to our community in which all visits are pay out-of-pocket, no insurance required. 
Gardner Express Care specializes in Weight Management, DOT Physicals, and Urgent Care. All services are self-pay, and extremely affordable!

Address: 1701 W 26th St, Ste G, Joplin, MO 64804, USA

Phone: 417-952-2369
